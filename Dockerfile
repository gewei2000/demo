FROM openjdk:8u212-jre-alpine3.9
RUN mkdir -p /app
ADD ./target/demo-1.0-SNAPSHOT.jar /app

EXPOSE 8089
ENTRYPOINT exec java $JAVA_OPTS -jar -Denv=DEV /app/demo-1.0-SNAPSHOT.jar  --spring.profiles.active=dev --server.port=80
