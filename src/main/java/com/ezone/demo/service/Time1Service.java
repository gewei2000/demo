package com.ezone.demo.service;

import java.util.Date;

import org.springframework.stereotype.Service;

/**
 * @author zhangdaoping
 * @create 2018/12/1 下午7:02
 */
@Service
public class Time1Service {

    /**
     * 获取时间
     *
     * @return
     */
    public String getTime(boolean result) {
        if (result) {
            return new Date().toString() + String.valueOf(result) + "111";
        } else {
            return new Date().toString() + String.valueOf(result) + "222";
        }
    }

    public String getTime1(boolean result) {
        return new Date().toString() + String.valueOf(result) + "111";
    }

}
