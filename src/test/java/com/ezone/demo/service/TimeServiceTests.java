package com.ezone.demo.service;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author zhangdaoping
 * @create 2018/12/1 下午7:03
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TimeServiceTests {

    @Resource
    private TimeService timeService;

    @Test
    public void getTimeTest() {
        boolean test = true;
        timeService.getTime(test);
        test = false;
        timeService.getTime(test);
    }
}
